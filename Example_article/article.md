# Example article

## Example text
This is an example article.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum neque egestas congue quisque egestas diam in arcu cursus. Massa tincidunt dui ut ornare lectus sit amet. Tincidunt vitae semper quis lectus. Sed risus pretium quam vulputate. Elit sed vulputate mi sit amet mauris. Neque convallis a cras semper auctor. Vitae justo eget magna fermentum iaculis eu non diam. Fames ac turpis egestas integer eget aliquet nibh. Quam lacus suspendisse faucibus interdum posuere lorem ipsum. Massa enim nec dui nunc mattis enim ut tellus. Sed vulputate odio ut enim blandit volutpat maecenas. Duis tristique sollicitudin nibh sit amet commodo. Aliquam sem et tortor consequat id porta nibh venenatis cras. Nisl pretium fusce id velit ut tortor pretium viverra suspendisse. Ullamcorper velit sed ullamcorper morbi tincidunt ornare massa eget.

At quis risus sed vulputate odio ut enim. Cursus sit amet dictum sit amet justo. Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. At consectetur lorem donec massa sapien faucibus et molestie. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Neque volutpat ac tincidunt vitae semper quis lectus nulla. Ac felis donec et odio pellentesque diam. Eu volutpat odio facilisis mauris sit. Cras adipiscing enim eu turpis. Accumsan tortor posuere ac ut. Et tortor consequat id porta nibh venenatis. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Magna fermentum iaculis eu non diam. Tellus in hac habitasse platea dictumst vestibulum rhoncus. Interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit. At tellus at urna condimentum mattis pellentesque id. Magna etiam tempor orci eu lobortis elementum nibh tellus molestie. Nulla pharetra diam sit amet.

<span style="color:red">This text is red,</span> <span style="text-decoration:underline">and this text is underlined.</span>

## Example images
![First example image](data/media/images/firstimage.png)

![Second example image](data/media/images/SecondImage.jpg)

![Third example image](data/media/images/third_image.gif)

## Example videos
[![How to network together all the world's encyclopedias](data/media/images/video-preview-1.jpg)](https://www.youtube.com/watch?v=lZc84J2lpIs "How to network together all the world's encyclopedias")

[![Big Tech took over our digital lives. Let's do something about it.](data/media/images/video-preview-2.jpg)](https://www.youtube.com/watch?v=1yNQJCCqs7c "Big Tech took over our digital lives. Let's do something about it.")

[![Why You Should Help Build the Encyclosphere](data/media/images/video-preview-3.jpg)](https://www.youtube.com/watch?v=RKkCEtZtl1A "Why You Should Help Build the Encyclosphere")